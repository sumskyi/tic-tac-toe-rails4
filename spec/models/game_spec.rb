require 'spec_helper'


describe Game do
  describe '#recent' do
    before do
      10.times do
        Game.create(:winner => %w(X O Draw).sample)
      end
    end

    let(:recent5) do
      Game.recent(5)
    end

    it 'fetch recent records' do
      recent5.first.id.should > recent5.last.id
    end
  end
end
