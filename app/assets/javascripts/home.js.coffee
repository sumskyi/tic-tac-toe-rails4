class @Point
  constructor: (@x, @y) ->

class @Home
  constructor: (board) ->
    @current_player_sign = 'X'
    @board = $(board)
    @init_board()
    @listen_steps()

  init_board: ->
    $.get '/home/current_game.json', (data)=>
      @board_data = data['board']['board']
      _.each @board_data, (row, row_idx) =>
        _.each row, (cell, col_idx) =>
          if cell
            @draw_sign(cell, new Point(row_idx, col_idx))
            return
        return

      return

  listen_steps: ->
    self = this
    @board.find('td').click ->
      $(@).addClass self.current_player_sign

      $.post '/home/step.json',
        current_player: self.current_player_sign,
        x: $(@).data('pos-x'),
        y: $(@).data('pos-y')
        (data) ->
          if data['winner']
            alert("And the winner is #{data['winner']['sign']}")
            location.href = '/home/reset'
          else if data['finished']
            alert("Draw game")
            location.href = '/home/reset'

          return


      self.board.removeClass("cursor-#{self.current_player_sign}")
      self.next_player()
      $(@).removeClass self.current_player_sign
      self.board.addClass("cursor-#{self.current_player_sign}")

      return
    return

  draw_sign: (value, point) ->
    td = @board.find("td[data-pos-x='#{point.x}'][data-pos-y='#{point.y}']")
    td.toggleClass(value)
    return

  next_player: ->
    if @current_player_sign == 'X'
      @current_player_sign = 'O'
    else
      @current_player_sign = 'X'
    return



$(document).ready ->
  for container in $('#board')
    new Home(container)
  return
