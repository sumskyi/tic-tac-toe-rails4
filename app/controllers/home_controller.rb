require 'tic_tac_toe'

class HomeController < ApplicationController
  respond_to :html, :json

  TOP_GAMES_COUNT = 10

  def index
  end

  def current_game
    render :json => game.to_json
  end

  def step
    game.turn(point, current_player)

    finished, winner = game.finished?, game.winner

    if finished
      winner_sign = winner ? winner.sign : 'draw'
      Game.create!(:winner => winner_sign)
    end

    render :json => {
      :finished => finished,
      :winner => winner
    }.to_json
  end

  def reset
    session.delete(:game)
    @_game = nil
    redirect_to root_path
  end

  def recent_games
    @games = Game.recent(TOP_GAMES_COUNT)
  end

private

  def point
    TicTacToe::Point.new(params[:x].to_i, params[:y].to_i)
  end

  def game
    @_game ||= begin
      session[:game] ||= TicTacToe::Game.new
    end
  end

  def current_player
    game.players[params[:current_player].intern]
  end
end
