class Game < ActiveRecord::Base
  scope :recent, ->(num) do
    order(:created_at => :desc).limit(num)
  end
end
